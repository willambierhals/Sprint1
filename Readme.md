<div align="center">
    <br>
    ![Logo do Sprint](img/logoProjeto.png)
    <h1 align="center"> Projeto do programa da Compass  </h1>
    <p align="center">
        <a href="#-sobre">Sobre</a> | 
	<a href="#-sobre">Branch</a>
    </p>
    <p align="center"> 
        Automação de testes em Java.
    </p>
    
</div>

## ✨ Sobre
- O projeto será realizado ao longo de três meses e será divido em 6 sprints com duração de duas semanas cada. Em cada sprint, terá um Challenge diferente e específico para cada conteúdo.<br>
- Este repositório será divido em branchs com pastas e cada uma representará uma sprint, dentro destas pastas terá uma divisão dos dias de cada sprint.
<br>

## ✴ Branch
Cada sprint deste programa está dividido em branchs separadas, para ter acesso a essas branchs, clique nos links abaixo:
- [sprint1](https://gitlab.com/willambierhals/compass/-/tree/pb_sprint1/sprint01).
- [sprint2](https://gitlab.com/willambierhals/compass/-/tree/pb_sprint2/sprint02).
- [sprint3](https://gitlab.com/willambierhals/compass/-/tree/pb_sprint3/sprint03).
- [sprint4](https://gitlab.com/willambierhals/compass/-/tree/pb_sprint4/sprint04).
- [sprint5](https://gitlab.com/willambierhals/compass/-/tree/pb_sprint5/sprint05).
- [sprint6](https://gitlab.com/willambierhals/compass/-/tree/pb_sprint6/sprint06).

<br><br>
by [William Bierhals](https://gitlab.com/willambierhals).
